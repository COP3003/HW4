/**

 * Lab 5: Words frequency
 * Author: UIN_9116
 * 12/11/2017 last modified
 */
/*
TODO: reset file path before submit
TODO: rename \bdict\b to dictionary
TODO: clean up imports
TODO: remove log method
 */
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.regex.Matcher;

class FileStats{
  private Scanner input;
  private ArrayList <String> wordList=new ArrayList<String>();
  private HashSet <String> wordSet=new HashSet<String>();
  private ArrayList <Entry<String>> entryList=new ArrayList<Entry<String>>();
  private Map <String, Character> dictionary =new TreeMap<String, Character>();

  private class Entry <T> implements Comparable<Entry<T>>{
    public T word;
    public int frequency;
    public Entry(T word, int f){
      this.word=word;
      frequency=f;
    }
    public int compareTo(Entry<T> e){
      // if this is larger returns > 0
      // if equal returns 0
      // if this is less than e returns < 0
      return e.frequency - this.frequency;
    }
  }

  public FileStats(String path) {
    try {
      input = new Scanner(new File(path));
    } catch(FileNotFoundException e) {
      System.out.println("Error opening file.");
      System.exit(1);
    }
    try {
      while (input.hasNextLine()) {

        // Difference between tokens is determined by spaces
        StringTokenizer st = new StringTokenizer(input.nextLine(), " ");
        while (st.hasMoreTokens()) {

          // add individual words to wordList
          wordList.add(st.nextToken().toLowerCase());
        }
      }
    } catch (NoSuchElementException e) {
      // no more lines in the file
      // no handler is necessary
    }

    for (int i = 0; i < wordList.size(); i++) {

      // add words without punctuation to wordList
      if (wordList.get(i).contains(",|;|.|'")) {
        wordList.set(i, wordList.get(i).substring(0, wordList.get(i).length() - 1));
      }
    }

    // add newList elements to wordSet
    wordSet.addAll(wordList);
    count();
  }

  /*
   * This method is supposed to
   * 1. find the frequency of each word in the file.
   * 2. display the four most frequent words and their frequencies.
	 * 3. construct the dictionary that four key-value pairs. The keys
	 *    are the most frequent words and the values are the characters,
	 *    used to represent the words.
   */
  private void count() {

    // declare frequency
    int frequency;

    // iteration
    for (String word : wordSet) {
      frequency = Collections.frequency(wordList, word);
      entryList.add(new Entry<>(word, frequency));

    }

    Collections.sort(entryList);

    Character[] symbols = new Character[]{'#','$','*','%'};

    // Collect four most used words in text to dictionary
    for (int i = 0; i < 4; i++) {
      dictionary.put(entryList.get(i).word, symbols[i]);
    }
  }

  public Map<String, Character> getDictionary(){
    return dictionary;
  }

  // prints the key-value pairs in the dictionary
  public void printDictionary(){
    for (String word : dictionary.keySet()) {
      System.out.println("(" + word + ", " + dictionary.get(word) + ")");
    }
  }
}

class FileCompressor{
  public static void compress(String src, String dest,
                              Map<String, Character> dict){

		/* insert your code here */
    Scanner input;
    StringBuilder data = new StringBuilder(1096);

    try {
      input = new Scanner(new File(src));
      List<String> lines = new ArrayList<>();
      while (input.hasNextLine()){
        lines.add(input.nextLine().toLowerCase());
      }

      for (int i = 0; i < lines.size(); i++){
        if (!lines.get(i).isEmpty()){
          for (String key : dict.keySet()){
            data.append(lines.get(i).replaceAll("\\b"+key+"\\b",Matcher.quoteReplacement(dict.get(key).toString())));
            lines.set(i, data.toString());
            data.setLength(0);
          }
        } else {
          lines.set(i, "\n\n");
        }
      }

      for (String l : lines) {
        data.append(l);
      }
      Files.write(Paths.get(dest),data.toString().getBytes());

    } catch (IOException e){
      System.out.println("Error opening file.");
      System.exit(1);
    }
  }

  public static void decompress(String src, String dest,
                                Map<Character, String> dict){

		/* insert your code here */
    Scanner input;
    StringBuilder data = new StringBuilder(1096);

    try {
      input = new Scanner(new File(src));
      List<String> lines = new ArrayList<>();
      while (input.hasNextLine()){
        lines.add(input.nextLine().toLowerCase());
      }

      for (int i = 0; i < lines.size(); i++){
        if (!lines.get(i).isEmpty()){
          for (Character key : dict.keySet()){
            data.append(lines.get(i).replaceAll("["+key.toString()+"]",Matcher.quoteReplacement(dict.get(key))));
            lines.set(i, data.toString());
            data.setLength(0);
          }
        } else {
          lines.set(i, "\n\n");
        }
      }

      for (String l : lines) {
        data.append(l);
      }
      Files.write(Paths.get(dest),data.toString().getBytes());

    } catch (IOException e){
      System.out.println("Error opening file.");
      System.exit(1);
    }
  }
}

public class FileIO {
  public static void main(String args[]) throws IOException{
    FileStats fs=new FileStats("basketball.txt");
    fs.printDictionary();

    Map <String, Character> m1=fs.getDictionary();
    FileCompressor.compress("basketball.txt",
        "compressed.txt",m1);

		/* insert your code here */
    /* create another dictionary for decompress and name it m2 */
    Map <Character,String> m2=new TreeMap<>();
    fs.getDictionary().forEach((s,c)->m2.put(c,s));
    FileCompressor.decompress("compressed.txt",
        "decompressed.txt",m2);
  }
}