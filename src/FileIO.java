/**
 * FileName: FileIO.java
 * Author: 5153
 * Created: December 4, 2017
 * Last Modified: December 11, 2017
 */
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

class FileStats{
  private Scanner input;
  private ArrayList <String> wordList=new ArrayList<String>();
  private HashSet <String> wordSet=new HashSet<String>();
  private ArrayList <Entry<String>> entryList=new ArrayList<Entry<String>>();
  private Map <String, Character> dictionary=new TreeMap<String, Character>();

  private class Entry <T> implements Comparable<Entry<T>>{
    public T word;
    public int frequency;
    public Entry(T word, int f){
      this.word=word;
      frequency=f;
    }
    public int compareTo(Entry<T> e){
      // if this is larger returns > 0
      // if equal returns 0
      // if this is less than e returns < 0
      return e.frequency - this.frequency;
    }
  }

  public FileStats(String path) {
    try {
      input = new Scanner(new File(path));
    } catch(FileNotFoundException e) {
      System.out.println("Error opening file..");
      e.printStackTrace();
      System.exit(1);
    }
    while(input.hasNextLine()){
      // separates each line into words and converts them to lowercase
      wordList.addAll(Arrays.stream(input.nextLine().split("\\W+"))
          .filter(t->!t.equals(""))
          .map(String::toLowerCase)
          .collect(Collectors.toList()));
    }
    wordSet.addAll(wordList);  // stores each unique word
    count();  // populates entryList and prints the top 4 words
  }

  /*
   * This method is supposed to
   * 1. find the frequency of each word in the file.
   * 2. display the four most frequent words and their frequencies.
	 * 3. construct the dictionary that four key-value pairs. The keys
	 *    are the most frequent words and the values are the characters,
	 *    used to represent the words.
   */
  private void count() {
    // create an Entry for each unique word and adds it to entryList
    wordSet.forEach(word->entryList
        .add(new Entry<>(word, Collections.frequency(wordList,word))));
    Collections.sort(entryList);  // sort words by most frequent

    // populate the dictionary with most frequent words and character values
    IntStream.range(0,4).forEach(i->dictionary.put(
                        entryList.get(i).word,"#$*%".charAt(i)));
  }

  public Map<String, Character> getDictionary(){
    return dictionary;
  }

  /*
    prints the key-value pairs in the dictionary such as
    (the, %)
    (is, *)
    (basketball, $)
    (and, ‘#’)
  */
  public void printDictionary(){
    dictionary.keySet().forEach(
        k-> System.err.println("(" + k + ", " + dictionary.get(k) + ")"));
  }
}

/*
 * Unfortunately Java is a hideous language bloated from incompetence and
 * scarred from decades of abuse. I tried with my limited capabilities to make
 * this class not ugly, but I failed. If in this code you can not see how
 * repulsive it is then please disregard and continue in blissful ignorance.
 */
class FileCompressor{
  public static void compress(String src, String dest,
                              Map<String, Character> dict){

		/* insert your code here */
    try {
      // create the regex pattern for compression
      String pattern = dict.keySet().stream()
          .collect(Collectors.joining("|", "\\b(", ")\\b"));
      fileInFileOut(src,dest,dict,pattern); // call the alteration method
    } catch (IOException e){  // catch all the errors here
      System.out.println("Error opening file.");
      System.exit(1);
    }
  }

  public static void decompress(String src, String dest,
                                Map<Character, String> dict){

		/* insert your code here */
		try {
      // create the regex pattern for decompression
		  String pattern = dict.keySet().stream()
          .map(String::valueOf)
          .collect(Collectors.joining("", "([", "])"));
      fileInFileOut(src,dest,dict,pattern); // call the alteration method
    } catch (IOException e){  // catch all the errors here
      System.out.println("Error opening file.");
      System.exit(1);
    }
  }

  /**
   * The magic happens here. This reads from a file, performs pattern matches,
   *  then Map key-value based string-to-string replacement and finally writes
   *  the changes to a file.
   * @param src String  path to input file (if relative, from source file dir.)
   * @param dest  String  path to output file. Creates or overwrites.
   * @param dictionary  Map key-values are converted to Strings, key is replaced
   *                      by value.
   * @param pattern String  keys from the dictionary to search the file for
   * @param <C> String-ish  something that can be converted to a String
   * @param <S> String-ish  something that can be converted to a String
   * @throws IOException  exceptions are handled by the calling method
   */
  private static<C, S> void fileInFileOut(String src, String dest,
                                    Map<C, S> dictionary, String pattern)
                                    throws IOException {

    // Let's get rid of the specialized maps and replace with a generalized map.
    Map <String,String> dict=new TreeMap<>();
    dictionary.forEach((c,s)->dict.put(c.toString(),s.toString()));

    // Open our source file as a stream. The stream is buffered to the next
    //  new line character.
    try (Stream<String> lines = Files.lines(Paths.get(src))) {

      // Reads the file content one line at a time. Each line is converted to
      //  lower case then added to a list. New line characters are stripped
      //  during this process.
      List<String> linesArray = new ArrayList<>();
      lines.map(String::toLowerCase).forEachOrdered(linesArray::add);

      // Creates a buffer and opens (or creates) the destination file.
      try (BufferedWriter data = new BufferedWriter(new FileWriter(dest), 1024)) {

        // Create our regex string from the dictionary keys. Looks something
        //  like: "([$#&*])" or "\\b(the|is|basketball|and)\\b". This
        //  returns whatever's found in a group which is used as the dict key.
        Pattern p = Pattern.compile(pattern);
        Matcher m = p.matcher("");  // create a reusable matcher

        int lineIndex;  // tracks whats has already been written to the buffer
        for (String line : linesArray) {
          if (!line.isEmpty()) {  // skip empty lines...
            m.reset(line);  // load the current line into the matcher
            lineIndex = 0;  // reset the position index for the new line
            while (m.find()) {  // if a match was found, load it into the group

              // add everything left of this match to the buffer
              data.append(line.substring(lineIndex, m.start()));

              // uses the match to add our conversion value from dict to buffer
              data.append(dict.get(m.group()));
              lineIndex = m.end();  // the index following our match
            }

            // write everything after the final match to the buffer
            data.append(line.substring(lineIndex));
          } else {
            data.newLine(); // end of a paragraph gets two new lines
            data.newLine();
          }
        }
        data.flush(); // write the rest of the buffer to the file
      }
    }
  }
}

public class FileIO {
  public static void main(String args[]) throws IOException{
    FileStats fs=new FileStats("basketball.txt");
    fs.printDictionary();

    Map <String, Character> m1=fs.getDictionary();
    FileCompressor.compress("basketball.txt","compressed.txt",m1);

		/* insert your code here */
    /* create another dict for decompress and name it m2 */
    Map <Character,String> m2=new TreeMap<>();
    fs.getDictionary().forEach((s,c)->m2.put(c,s));
    FileCompressor.decompress("compressed.txt","decompressed.txt",m2);
  }
}